//Problem 1.	Sort Array of Numbers

int[] arr = new int[] { 6, 5, 4, 10, -3, 120, 4};
int[] arr1 = new int[] { 10, 9, 8};
Array.Sort(arr);
Array.Sort(arr1);
arr.ToList().ForEach(a => Console.Write(a));
arr1.ToList().ForEach(a => Console.Write(a));

//Problem 2.	Sort Array of Numbers Using Selection Sort

 static int[] SelectionSort(int[] arr)
    {
        int min = 0;
        for (int i = 0; i < arr.Length; i++)
        {
            min = i;
            for (int j = i; j < arr.Length; j++)
            {
                if (arr[j] < arr[min])
                    min = j;
            }
            int _temp = arr[i];
            arr[i] = arr[min];
            arr[min] = _temp;
        }
        return arr;
    }
	static void printArr(int[] array)
    {
        for (int i = 0; i < array.Length; i++) // print sorted array
        {
            Console.Write(array[i] + " ");
        }
        Console.WriteLine();
    }

	static void Main(string[] args)
    {
        int[] array = Console.ReadLine().Split(' ').Select(int.Parse).ToArray();
        SelectionSort(array);
        printArr(array);
    }
	
//Problem 3.	Categorize Numbers and Find Min / Max / Average

string inputSt = Console.ReadLine();
        string[] inArrString =
            inputSt.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
        decimal[] numbers = new decimal[inArrString.Length];
        List<decimal> floatPointNumbers = new List<decimal>() { };
        List<decimal> integerNumbers = new List<decimal>() { };

        for (int i = 0; i < numbers.Length; i++)
        {
            numbers[i] = decimal.Parse(inArrString[i]);
            if (numbers[i] % 1 != 0)
            {
                floatPointNumbers.Add(numbers[i]);
            }
            else
            {
                integerNumbers.Add(numbers[i]);
            }
        }

        Console.WriteLine("[{0}] -> min: {1:F2}, max: {2:F2}, sum: {3:F2}, avg: {4:F2}",
            string.Join(", ", floatPointNumbers), floatPointNumbers.Min(), floatPointNumbers.Max(),
                       floatPointNumbers.Sum(), floatPointNumbers.Average());

        Console.WriteLine("[{0}] -> min: {1:F2}, max: {2:F2}, sum: {3:F2}, avg: {4:F2}",
            string.Join(", ", integerNumbers), integerNumbers.Min(), integerNumbers.Max(),
                       integerNumbers.Sum(), integerNumbers.Average());

//Problem 4.	Sequences of Equal Strings

string[] input = Console.ReadLine().Split();
    
        Console.Write("{0} ", input[0]);
        for (int i = 1; i < input.Length; i++)
        {
            if (input[i] == input[i - 1])
            {
                Console.Write("{0} ", input[i]);
            }
            else
            {
                Console.Write("\n\r{0} ", input[i]);
            }
        }
        Console.WriteLine();

//Problem 5.	Longest Increasing Sequence		

		int[] input = Console.ReadLine().Split().Select(int.Parse).ToArray();

        var currentSequence = new List<int>();
        var longest = new List<int>();
        var sequence = new List<int>();

        int prev;
        int next;
        bool isIncr = false;

        for (int i = 0; i < input.Length; i++)
        {
            prev = input[i];
            next = input[Math.Min((i + 1), input.Length - 1)];
            if (prev < next)
            {
                Console.Write("{0} ", prev);
                isIncr = true;
            }
            else
            {
                Console.WriteLine("{0} ", input[i]);
            }

            if (isIncr)
            {
                currentSequence.Add(input[i]);
            }
            else
            {
                currentSequence.Add(input[i]);
                if (longest.Count < currentSequence.Count)
                {
                    longest.Clear();
                    for (int j = 0; j < currentSequence.Count; j++)
                    {
                        longest.Add(currentSequence[j]);
                    }
                }
                currentSequence.Clear();
            }
            isIncr = false;
        }
        Console.WriteLine("Longest: {0}", string.Join(" ", longest));