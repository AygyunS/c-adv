//Problem 1.	Bigger Number

 static int GetMaxNum(int firstNumber, int secondNumber)
    {
        if (firstNumber >= secondNumber)
        {
            return firstNumber;
        }
        else
        {
            return secondNumber;
        }
        return firstNumber;
    }


    static void Main(string[] args)
    {
        int firstNumber = int.Parse(Console.ReadLine());
        int secondtNumber = int.Parse(Console.ReadLine());
        int max = GetMaxNum(firstNumber, secondtNumber);
        Console.WriteLine(max);

    }

//Problem 2.	Last Digit of Number

static string GetLastDigitAsWord(int number)
    {
        int lastDigit = number%10;
        string digitAsWord = null;
        switch (lastDigit)
        {
            case 0:
                digitAsWord = "zero";
                break;
            case 1:
                digitAsWord = "one";
                break;
            case 2:
                digitAsWord = "two";
                break;
            case 3:
                digitAsWord = "three";
                break;
            case 4:
                digitAsWord = "four";
                break;
            case 5:
                digitAsWord = "five";
                break;
            case 6:
                digitAsWord = "six";
                break;
            case 7:
                digitAsWord = "seven";
                break;
            case 8:
                digitAsWord = "eight";
                break;
            case 9:
                digitAsWord = "nine";
                break;
            default:
                Console.WriteLine("Error!");
                break;
        }
        return digitAsWord;
    }


    static void Main(string[] args)
    {
        Console.WriteLine(GetLastDigitAsWord(512));
        Console.WriteLine(GetLastDigitAsWord(1024));
        Console.WriteLine(GetLastDigitAsWord(12309));
    }

//Problem 3.	Larger Than Neighbours

static bool IsLargerThanNeighbours(int[] numbers, int i)
    {
        bool numberIslarger = false;
        if (numbers[i] != numbers[0] && numbers[i] != numbers[numbers.Length - 1])
        {
            if (numbers[i] > numbers[i - 1] && numbers[i] > numbers[i + 1])
            {
                numberIslarger = true;
            }
        }
        else
        {
            if (numbers[i] == numbers[0])
            {
                if (numbers[0] > numbers[1])
                {
                    numberIslarger = true;
                }
            }
            else if (numbers[i] == numbers[numbers.Length - 1])
            {
                if (numbers[numbers.Length - 1] > numbers[numbers.Length - 2])
                {
                    numberIslarger = true;
                }
            }
        }
        return numberIslarger;
    }


    static void Main(string[] args)
    {
        int[] numbers = new int[] {1, 3, 4, 5, 1, 0 , 5};
        for (int i = 0; i < numbers.Length; i++)
        {
            Console.WriteLine(IsLargerThanNeighbours(numbers, i));
        }

    }
	
//Problem 4.	First Larger Than Neighbours

static int GetIndexOfFirstElementIsLargerThanNeighbours(int[] numbers)
    {
        int indexOfFirstNumber = -1;
        for (int i = 0; i < numbers.Length; i++)
        {
            if (numbers[i] != numbers[0] && numbers[i] != numbers[numbers.Length - 1])
            {
                if (numbers[i] > numbers[i - 1] && numbers[i] > numbers[i + 1])
                {
                    return indexOfFirstNumber = i;
                }
            }
            else
            {
                if (numbers[i] == numbers[0])
                {
                    if (numbers[0] > numbers[1])
                    {
                        return indexOfFirstNumber = i;
                    }
                }
                else if (numbers[i] == numbers[numbers.Length - 1])
                {
                    if (numbers[numbers.Length - 1] > numbers[numbers.Length - 2])
                    {
                        return indexOfFirstNumber = i;
                    }
                }
            }
        }
        return indexOfFirstNumber;
    }


    static void Main(string[] args)
    {
        int[] sequenceOne = new int[] { 1, 3, 4, 5, 1, 0, 5 };
        int[] sequenceTwo = new int[] { 1, 2, 3, 4, 5, 6, 6 };
        int[] sequenceThree = new int[] { 1, 1, 1 };

        Console.WriteLine(GetIndexOfFirstElementIsLargerThanNeighbours(sequenceOne));
        Console.WriteLine(GetIndexOfFirstElementIsLargerThanNeighbours(sequenceTwo));
        Console.WriteLine(GetIndexOfFirstElementIsLargerThanNeighbours(sequenceThree));


    }

//Problem 5.	Reverse Number

static double GetReversedNumber(double number)
    {
        string reversedString = new string(number.ToString().Reverse().ToArray());
        double reversedNumber = double.Parse(reversedString);
        return reversedNumber;
    }


    static void Main(string[] args)
    {
        double reversedOne = GetReversedNumber(256);
        double reversedTwo = GetReversedNumber(123.45);
        double reversedThree = GetReversedNumber(0.12);

        Console.WriteLine(reversedOne);
        Console.WriteLine(reversedTwo);
        Console.WriteLine(reversedThree);

    }

//Problem 6.	Number Calculations

static double Sum(params double[] set)
    {
        double sum = 0;
        for (int i = 0; i < set.Length; i++)
        {
            sum += set[i];
        }
        return sum;
    }
    static double Average(params double[] set)
    {
        double sum = 0;
        for (int i = 0; i < set.Length; i++)
        {
            sum += set[i];
        }
        double average = sum / set.Length;
        return average;
    }
    static double Product(params double[] set)
    {
        double product = 1;
        for (int i = 0; i < set.Length; i++)
        {
            product *= set[i];
        }
        return product;
    }
    static double Min(params double[] set)
    {
        double min = double.MaxValue;
        for (int i = 0; i < set.Length; i++)
        {
            if (min > set[i])
            {
                min = set[i];
            }
        }
        return min;
    }
    static double Max(params double[] set)
    {
        double max = double.MinValue;
        for (int i = 0; i < set.Length; i++)
        {
            if (max < set[i])
            {
                max = set[i];
            }
        }
        return max;
    }
    static int Sum(params int[] set)
    {
        int sum = 0;
        for (int i = 0; i < set.Length; i++)
        {
            sum += set[i];
        }
        return sum;
    }
    static float Average(params int[] set)
    {
        int sum = 0;
        for (int i = 0; i < set.Length; i++)
        {
            sum += set[i];
        }
        float average = (float)sum / set.Length;
        return average;
    }
    static int Product(params int[] set)
    {
        int product = 1;
        for (int i = 0; i < set.Length; i++)
        {
            product *= set[i];
        }
        return product;
    }
    static int Min(params int[] set)
    {
        int min = int.MaxValue;
        for (int i = 0; i < set.Length; i++)
        {
            if (min > set[i])
            {
                min = set[i];
            }
        }
        return min;
    }
    static int Max(params int[] set)
    {
        int max = int.MinValue;
        for (int i = 0; i < set.Length; i++)
        {
            if (max < set[i])
            {
                max = set[i];
            }
        }
        return max;
    }

    static void Main(string[] args)
    {
        Console.Write("Insert set of numbers separated by a space: ");
        var numbers = Console.ReadLine().Split().Select(double.Parse).ToArray();

        HashSet<double> setDouble = new HashSet<double>();

        for (int i = 0; i < numbers.Length; i++)
        {
            setDouble.Add(numbers[i]);
        }

        double sum = Sum(numbers);
        double average = Average(numbers);
        double product = Product(numbers);
        double min = Min(numbers);
        double max = Max(numbers);

        Console.WriteLine("The sum of numbers is: {0}", sum);
        Console.WriteLine("The average of numbers is: {0}", average);
        Console.WriteLine("The product of numbers is: {0}", product);
        Console.WriteLine("The min number is: {0}", min);
        Console.WriteLine("The max number is: {0}", max);

        var setInt = new int[] { 1, 2, 3, 4 };

        int sumInt = Sum(setInt);
        float averageInt = Average(setInt);
        int productInt = Product(setInt);
        int minInt = Min(setInt);
        int maxInt = Max(setInt);

        Console.WriteLine();
        Console.WriteLine("Sum {0}; Average: {1}, Product: {2};  Min: {3}; Max: {4};", sumInt, averageInt, productInt, minInt, maxInt);
    }