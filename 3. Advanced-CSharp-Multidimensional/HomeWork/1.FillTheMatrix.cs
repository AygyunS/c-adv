static void PrintMatrix(int[,] matrix)
    {
        for (int row = 0; row < matrix.GetLength(0); row++)
        {
            for (int col = 0; col < matrix.GetLength(1); col++)
            {
                Console.Write("{0,-4}", matrix[row, col]);
            }
            Console.WriteLine();
        }
    }

    static int[,] matirxOne(int[,] matrix)
    {
        int count = 1;

        for (int col = 0; col < matrix.GetLength(1); col++)
        {
            for (int row = 0; row < matrix.GetLength(0); row++)
            {
                matrix[row, col] = count;
                count++;
            }
        }


        return matrix;
    }
    static int[,] matirxTwo(int[,] matrix)
    {
        int count = 1;

        for (int col = 0; col < matrix.GetLength(1); col++)
        {
            for (int row = 0; row < matrix.GetLength(0); row++)
            {
                if (col%2 == 0)
                {
                    matrix[row, col] = count;
                    count++;
                }
                else
                {
                    matrix[(matrix.GetLength(0) - 1) - row, col] = count;
                    count++;
                }

            }
        }


        return matrix;
    }

   

    static void Main()
    {
        int n = Int32.Parse(Console.ReadLine());

        var firstMatrix = new int[n, n];
        var secondMatrix = new int[n, n];

        matirxOne(firstMatrix);

        PrintMatrix(firstMatrix);
                
        Console.WriteLine();
        Console.WriteLine();

        matirxTwo(secondMatrix);
        PrintMatrix(secondMatrix);
    }