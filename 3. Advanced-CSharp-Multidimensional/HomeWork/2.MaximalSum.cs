static void PrintBestSumOfMatrix(int[,] matrix, int n, int m)
    {
        int count = 0;
        for (int row = n; row < matrix.GetLength(0) - 2; row++)
        {
            for (int col = m; col < matrix.GetLength(1) - 2; col++)
            {
                
                Console.WriteLine("{0} {1} {2}", matrix[row, col], matrix[row, col + 1], matrix[row, col + 2]);
                Console.WriteLine("{0} {1} {2}", matrix[row+1, col], matrix[row+1, col + 1], matrix[row+1, col + 2]);
                Console.WriteLine("{0} {1} {2}", matrix[row + 2, col], matrix[row + 2, col + 1], 
                    matrix[row + 2, col + 2]);
                count++;
                if (count == 1)
                {
                    goto End;
                }

            }
            Console.WriteLine();
        }
        End:
        ;
    }

    static void CreateMatrix(int[,] matrix)
    {
        for (int row = 0; row < matrix.GetLength(0); row++)
        {
            for (int col = 0; col < matrix.GetLength(1); col++)
            {
                string inputElement = Console.ReadLine();
                matrix[row, col] = int.Parse(inputElement);
            }
        }
    }

    static void Main()
    {
        int n = Int32.Parse(Console.ReadLine());
        int m = Int32.Parse(Console.ReadLine());

        var matrix = new int[n, m];
        CreateMatrix(matrix);
        Console.WriteLine();
        int bestSum = int.MinValue;
        int currentN = 0;
        int currentM = 0;
        for (int row = 0; row < matrix.GetLength(0) - 2; row++)
        {
            for (int col = 0; col < matrix.GetLength(1) - 2; col++)
            {
                int sum = matrix[row, col] + matrix[row, col + 1] + matrix[row, col + 2]
                          + matrix[row + 1, col] + matrix[row + 1, col + 1] + matrix[row + 1, col + 2]
                          + matrix[row + 2, col] + matrix[row + 2, col + 1] + matrix[row + 2, col + 2];
                if (sum > bestSum)
                {
                    bestSum = sum;
                    currentN = row;
                    currentM = col;
                }
                    
            }
        }
        Console.WriteLine(bestSum);
        PrintBestSumOfMatrix(matrix, currentN, currentM);
