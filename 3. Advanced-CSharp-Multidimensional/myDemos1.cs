static void PrintArr(int[,] arr)
    {
        for (int row = 0; row < arr.GetLength(0); row++)
        {
            for (int col = 0; col < arr.GetLength(1); col++)
            {
                Console.Write("{0,4}",arr[row, col]);
            }
            Console.WriteLine();
        }

    }
    static void CreateArr(int[,] arr)
    {
        int n = 1;
        for (int row = 0; row < arr.GetLength(0); row++)
        {
            for (int column = 0; column < arr.GetLength(1); column++)
            {
                arr[row, column] = n;
                n++;
            }
        }

    }
    static void Main(string[] args)
    {
        int rows = int.Parse(Console.ReadLine());
        int columns = int.Parse(Console.ReadLine());
        int[,] matrix = new int[rows, columns];
        CreateArr(matrix);
        PrintArr(matrix);
    }