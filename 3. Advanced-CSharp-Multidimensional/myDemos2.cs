static void PrintArr(int[,] arr)
    {
        for (int row = 0; row < arr.GetLength(0); row++)
        {
            for (int col = 0; col < arr.GetLength(1); col++)
            {
                Console.Write("{0,4}", arr[row, col]);
            }
            Console.WriteLine();
        }

    }

    static void CreateArr(int[,] arr)
    {
        int n = 1;
        for (int row = 0; row < arr.GetLength(0); row++)
        {
            for (int column = 0; column < arr.GetLength(1); column++)
            {
                arr[row, column] = n;
                n++;
            }
        }

    }

    static int MultiplyRowAndCols(int[,] matrixOne, int[,] matrixTwo, int row, int col)
    {
        int sum = 0;
        for (int i = 0; i < matrixOne.GetLength(1); i++)
        {
            sum += matrixOne[row, i] * matrixTwo[i, col];
        }
        return sum;
    }

    static void Main(string[] args)
    {
        int[,] matrixOne = { { 2, 3, 5 }, { 1, 5, 6 } };
        int[,] matrixTwo = { { 2, 3 }, { 1, 5 }, { 2, 3 } };

        if (matrixOne.GetLength(1) != matrixTwo.GetLength(0))
        {
            Console.WriteLine("Error matrix can't be multiplied!");
            return;
        }

        int[,] resultArr = new int[matrixOne.GetLength(0), matrixTwo.GetLength(1)];
        for (int row = 0; row < resultArr.GetLength(0); row++)
        {
            for (int col = 0; col < resultArr.GetLength(1); col++)
            {
                resultArr[row, col] = MultiplyRowAndCols(matrixOne, matrixTwo, row, col);
            }
        }
        PrintArr(resultArr);

    }