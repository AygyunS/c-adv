using System;
using System.Linq;
using System.Text;

class Program
{
    static string ReverseString(string str)
    {
        char[] arr = str.ToCharArray();
        Array.Reverse(arr);
        return new string(arr);
    }

    static void Main()
    {
        string inputString = Console.ReadLine();
        Console.WriteLine(ReverseString(inputString));
        

    }
}