using System;

class Program
{
    static void Main()
    {
        string inputString = Console.ReadLine();

        if (inputString.Length >= 20)
        {
            for (int i = 0; i < 20; i++)
            {
                Console.Write(inputString[i]);
            }
        }
        else
        {
            for (int i = 0; i < inputString.Length; i++)
            {
                Console.Write(inputString[i]);
                
            }
            Console.Write(new string('*', 20-inputString.Length));
        }
        Console.WriteLine();
    }
}