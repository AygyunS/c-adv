using System;

class Program
{
    static void Main()
    {
        string inputString = Console.ReadLine().ToLower();
        string subString = Console.ReadLine().ToLower();

        int count = 0;

        for (int index = 0; index < inputString.Length; index++)
        {
            int indexResulatat = inputString.IndexOf(subString, index);
            if (indexResulatat >= 0)
            {
                count++;
                index = indexResulatat + 1;
            }
            else
            {
                index = inputString.Length;
            }
        }
        Console.WriteLine(count);
    }
}