using System;
using System.Text;

class Program
{
    static void Main()
    {
        string input = Console.ReadLine();

        var unicodeChar = new StringBuilder();

        for (int character = 0; character < input.Length; character++)
        {
            unicodeChar.Append("\\u" + ((int)input[character]).ToString("X4"));
        }
        Console.WriteLine(unicodeChar.ToString());
    }
}