using System;

class Program
{
    static void Main()
    {
        string inputString = Console.ReadLine();
        string[] word = inputString.Split(new[] { ' ', ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
        string text = Console.ReadLine();

        for (int indexWordArr = 0; indexWordArr < word.Length; indexWordArr++)
        {
            text = text.Replace(word[indexWordArr], new string('*', word[indexWordArr].Length));
        }

        Console.WriteLine(text);

    }
}