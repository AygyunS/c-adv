		string text = "<ul>\r\n <li>\r\n  <a href=http://softuni.bg>SoftUni</a>\r\n </li>\r\n</ul>\r\n";
        string pattern = @"\<a\s[href]+\=[htp]+\:\/\/\w+\.\w+\>\w+\<\/a\>";
        string replacement = @"[URL href=http://softuni.bg]SoftUni[/URL]";

        Regex regex = new Regex(pattern);
        string result = regex.Replace(text, replacement);

        Console.WriteLine(result);